/*
sdmess main binary
 - invoked by systemd when a card is plugged: show notification
 - invoked by CLI
    - CRUD known cards
    - show connected devices
*/
use anyhow::{Context, Result};
use humantime::{format_duration, format_rfc3339};
use indicatif::{ProgressBar, ProgressStyle};
use inquire::{Confirm, Text};
use serde::{Deserialize, Serialize};
use std::collections::BTreeMap;
use std::fs;
use std::fs::File;
use std::io::BufReader;
use std::io::{self};
use std::path::Path;
use std::path::PathBuf;
use std::process;
use std::process::Command;
use std::time::Duration;
use std::time::SystemTime;
use structopt::StructOpt;

// TODO: disambiguate ptuuid vs fingerprint

// Datastore

#[derive(Serialize, Deserialize, Debug)]
struct DataItem {
    fingerprint: String,
    desc: String,
    creation_time: SystemTime,
    last_seen_time: SystemTime,
    protected: bool,
    size: String,
}

#[derive(Serialize, Deserialize, Debug)]
struct DataStore {
    v: u8,
    entries: BTreeMap<String, DataItem>,
}

fn load_datastore(datastore_fn: &PathBuf) -> DataStore {
    let file = File::open(datastore_fn).expect("Unable to open file");
    let reader = BufReader::new(file);
    serde_json::from_reader(reader).expect("Unexpected JSON structure")
}

fn atomic_write(data: DataStore, datastore_fn: &PathBuf) {
    let tmpf = datastore_fn.with_extension("tmp");
    let j = serde_json::to_string(&data).expect("");
    fs::write(&tmpf, j).expect("Unable to write file");
    fs::rename(&tmpf, datastore_fn).expect("Unable to rename file");
}

// lsblk parsing

#[derive(Deserialize, Debug)]
struct LsblkOut {
    blockdevices: Vec<BlockDevice>,
}

#[allow(dead_code)]
#[derive(Deserialize, Debug, Clone)]
struct BlockDevice {
    id: Option<String>,
    device_link: Option<String>,
    uuid: Option<String>,
    ptuuid: Option<String>,
    partuuid: Option<String>,
    parttype: Option<String>,
    rm: bool, // removable
    size: Option<String>,
    fstype: Option<String>,
    model: Option<String>,
    pttype: Option<String>,
    tran: Option<String>,
    // TODO: add size
    #[serde(rename = "maj:min")]
    major_minor: Option<String>,
}

fn list_devices() -> std::vec::Vec<BlockDevice> {
    let lb = Command::new("lsblk")
        .args([
            "--nodeps",
            "-o",
            "ID,UUID,PTUUID,PARTUUID,PARTTYPE,RM,SIZE,FSTYPE,MODEL,PTTYPE,TRAN,MAJ:MIN",
            "-J",
        ])
        .output()
        .expect("Unable to run lsblk");

    let output_str = String::from_utf8_lossy(&lb.stdout);
    let mut lsblk: LsblkOut =
        serde_json::from_str(&output_str).expect("Failed to parse the output from lsblk");
    for d in lsblk.blockdevices.iter_mut() {
        d.device_link = d.id.as_ref().map(|v| format!("/dev/disk/by-id/{}", v))
    }

    if cfg!(debug_assertions) {
        // Fake device for testing
        vec![BlockDevice {
            id: Some("devnull device: sdmess has been built in debug mode!".to_owned()),
            uuid: Some("dn uuid".to_owned()),
            device_link: Some("/dev/null".to_owned()),
            ptuuid: Some("devnull-ptuuid".to_owned()),
            partuuid: Some("devnull partuuid".to_owned()),
            parttype: Some("devnull parttype".to_owned()),
            rm: true,
            size: Some("Plenty!".to_owned()),
            fstype: Some("foo".to_owned()),
            model: Some("DevNull 9000 High Capacity".to_owned()),
            pttype: Some("".to_owned()),
            tran: Some("usb".to_owned()),
            major_minor: Some("dn major minor".to_owned()),
        }]
    } else {
        lsblk
            .blockdevices
            .into_iter()
            .filter(|d| d.rm && d.major_minor.is_some() && d.ptuuid.is_some())
            .collect()
    }
}

// friendly timestamp
fn friendlytime(t: SystemTime) -> String {
    let ts = &format_rfc3339(t).to_string()[..19].replace('T', " ");
    let dur = {
        let delta = t.elapsed().unwrap();
        if delta < Duration::new(1, 0) {
            return "now".to_string();
        }
        let dur = format_duration(delta).to_string();
        dur.split_whitespace().take(2).collect::<Vec<_>>().join(" ")
    };
    format!("{} ({})", ts, dur)
}

// list devices
fn cmd_list(datastore_fn: &PathBuf) {
    // TODO flag plugged device list_devices();
    let data = load_datastore(datastore_fn);
    if data.entries.is_empty() {
        println!("No entries");
        return;
    }
    println!(
        "{:40} {:31} {:31} {}",
        "Fingerprint", "Creation time", "Last seen time", "Description"
    );
    for i in data.entries.values() {
        println!(
            "{:40} {:31} {:31} {}",
            i.fingerprint,
            friendlytime(i.creation_time),
            friendlytime(i.last_seen_time),
            i.desc,
            //i.protected
        );
    }
}

fn print_full_dev(dev: &BlockDevice, db_entry: &DataItem) {
    println!(
        "
Partition ID:    {}
Major:minor:     {}
Model:           {}
ID:              {}
First time seen: {}
Last time seen:  {}
Size:            {}
Device path:     {}        
Description:     {}
                    ",
        dev.ptuuid.as_deref().unwrap_or(""),
        dev.major_minor.as_deref().unwrap_or(""),
        dev.model.as_deref().unwrap_or(""),
        dev.id.as_deref().unwrap_or(""),
        friendlytime(db_entry.creation_time),
        friendlytime(db_entry.last_seen_time),
        dev.size.as_deref().unwrap_or(""),
        dev.device_link.as_deref().unwrap_or(""),
        db_entry.desc,
    );
}

fn ask_then_add(fingerprint: &str, dev: &BlockDevice, datastore_fn: &PathBuf) {
    if let Ok(true) = Confirm::new("Add the device now?")
        .with_default(false)
        .prompt()
    {
        if let Ok(desc) = Text::new("Description:").prompt() {
            let mut data = load_datastore(datastore_fn);
            data.entries.insert(
                fingerprint.to_string(),
                DataItem {
                    fingerprint: fingerprint.to_string(),
                    desc,
                    creation_time: SystemTime::now(),
                    last_seen_time: SystemTime::now(),
                    protected: false,
                    size: dev.size.as_deref().unwrap_or("").to_string(),
                },
            );
            atomic_write(data, datastore_fn);
        } else {
            eprintln!("Unexpected error");
            process::exit(1)
        }
    }
}

fn gen_fingerprint(d: &BlockDevice) -> &str {
    d.ptuuid.as_deref().unwrap_or("")
}

// scan plugged devices
fn cmd_scan(datastore_fn: &PathBuf) {
    let devices = list_devices();
    if devices.is_empty() {
        println!("No devices detected.");
        return;
    }
    let mut data = load_datastore(datastore_fn);
    let mut datastore_updated = false;

    for d in &devices {
        let stored = data.entries.get_mut(d.ptuuid.as_ref().unwrap());
        match stored {
            Some(st) => {
                print_full_dev(d, st);
                st.last_seen_time = SystemTime::now();
                datastore_updated = true;
            }
            None => {
                let fingerprint = gen_fingerprint(d);

                println!(
                    "                  --[ unknown device ]--
  -[ run: sdmess add {} '<description>' ]-

Partition ID: {:40}
Major:minor:  {:20}
Model:        {:20}
ID:           {}
Size:         {}
",
                    fingerprint,
                    fingerprint,
                    d.major_minor.as_deref().unwrap_or(""),
                    d.model.as_deref().unwrap_or(""),
                    d.id.as_deref().unwrap_or(""),
                    d.size.as_deref().unwrap_or(""),
                );
                ask_then_add(fingerprint, d, datastore_fn);
            }
        }
    }
    if datastore_updated {
        atomic_write(data, datastore_fn);
    }
}

// fn cmd_notify(datastore_fn: &PathBuf) {
// TODO monitor and notify
// }

// add or update a device description
fn cmd_update(fingerprint: &str, desc: &str, datastore_fn: &PathBuf) {
    let mut data = load_datastore(datastore_fn);
    if let Some(i) = data.entries.get_mut(fingerprint) {
        i.desc = desc.to_owned()
    } else {
        eprintln!("Entry not found: plug the memory card and run `scan`");
        process::exit(1);
    }
    atomic_write(data, datastore_fn);
}

// delete device
fn cmd_delete(fingerprint: &str, datastore_fn: &PathBuf) {
    // let devices = list_devices();
    let mut data = load_datastore(datastore_fn);
    // TODO notice about non-plugged device
    data.entries.remove(fingerprint);
    atomic_write(data, datastore_fn);
}

fn find_plugged_dev(devices: Vec<BlockDevice>, fp: &str) -> BlockDevice {
    for d in &devices {
        if d.ptuuid == Some(fp.to_owned()) {
            return d.clone();
        }
    }
    println!("\nError: Card with fingerprint '{fp}' not inserted\n",);
    process::exit(1);
}

fn decompress_to_device(source: &mut File, target_dev_path: &str, format: &str) -> Result<()> {
    //
    let file_size = source.metadata().unwrap().len();
    let writer = &mut File::create(target_dev_path).unwrap();
    let pb = ProgressBar::new(file_size);
    let reader = &mut pb.wrap_read(source);
    pb.set_style(
        ProgressStyle::with_template(
            "[{elapsed}] {bar:60.cyan/blue} {bytes_per_sec} [{eta}] {msg}",
        )
        .unwrap(),
    );

    // TODO: check magic bytes
    match format {
        "bzip2" | "bz2" => {
            let mut decoder = bzip2::read::BzDecoder::new(reader);
            io::copy(&mut decoder, writer).context("Failed to decompress data")?;
        }
        "gzip" | "gz" => {
            let mut decoder = flate2::read::GzDecoder::new(reader);
            io::copy(&mut decoder, writer).context("Failed to decompress data")?;
        }
        "xz" => {
            let mut decoder = xz2::read::XzDecoder::new(reader);
            io::copy(&mut decoder, writer).context("Failed to decompress data")?;
        }
        "zst" | "zstd" => {
            let mut decoder = zstd::stream::read::Decoder::new(reader)?;
            io::copy(&mut decoder, writer).context("Failed to decompress data")?;
        }
        _ => {
            println!("Not compressed, copying raw data");
            io::copy(reader, writer).unwrap();
        }
    }
    pb.finish_with_message("Done :)");
    Ok(())
}

// flash image
fn cmd_flash_image(src_fname: &str, fingerprint: &str, datastore_fn: &PathBuf) {
    // TODO progress bar
    // TODO estimate decompressed size, check available space

    let devices = list_devices();
    let mut data = load_datastore(datastore_fn);

    // Check if the card is known in the DB and plugged
    let db_entry = match data.entries.get(fingerprint) {
        Some(d) => d,
        None => {
            println!("\nError: Card with fingerprint '{fingerprint}' is unknown. Please add it.\n",);
            process::exit(1);
        }
    };
    let dev = find_plugged_dev(devices, fingerprint);
    print_full_dev(&dev, db_entry);

    let ans = Confirm::new("Check the device path. Continue?")
        .with_default(false)
        .prompt();
    match ans {
        Ok(true) => (),
        Ok(false) | Err(_) => process::exit(0),
    }

    let mut file = File::open(src_fname).unwrap();
    let ftype = Path::new(src_fname).extension().unwrap().to_str().unwrap();
    decompress_to_device(&mut file, &dev.device_link.unwrap(), ftype).unwrap();

    data.entries.remove(fingerprint);
    atomic_write(data, datastore_fn);
    cmd_scan(datastore_fn);
}

/// SDmess: A utility for managing memory cards
#[derive(StructOpt, Debug)]
#[structopt(name = "sdmess")]
enum Cli {
    /// Lists all known devices.
    List,
    /// Scans for connected devices.
    Scan,
    /// Updates description for an existing device.
    Update {
        /// Partition UUID of the device to update.
        fingerprint: String,
        /// New description for the device.
        description: String,
    },
    /// Deletes a device.
    Delete {
        /// Partition UUID of the device to delete.
        fingerprint: String,
    },
    /// Flashes an image to a device.
    FlashImage {
        /// File name of the image to flash.
        img_filename: String,
        /// Partition UUID of the target device.
        fingerprint: String,
    },
}

fn main() {
    // XDG paths
    let xdg_dirs = xdg::BaseDirectories::with_prefix("sdmess").unwrap();
    let datastore_fn = &xdg_dirs
        .place_data_file("fingerprints.json")
        .expect("cannot create data directory");
    if !datastore_fn.exists() {
        println!("Creating data file at {:?}", datastore_fn);
        let data = DataStore {
            v: 1,
            entries: BTreeMap::new(),
        };
        atomic_write(data, datastore_fn);
    }

    let args = Cli::from_args();
    match args {
        Cli::List => cmd_list(datastore_fn),
        Cli::Scan => cmd_scan(datastore_fn),
        Cli::Update {
            fingerprint,
            description,
        } => {
            cmd_update(&fingerprint, &description, datastore_fn);
        }
        Cli::Delete { fingerprint } => {
            cmd_delete(&fingerprint, datastore_fn);
        }
        Cli::FlashImage {
            img_filename,
            fingerprint,
        } => {
            cmd_flash_image(&img_filename, &fingerprint, datastore_fn);
        } // TODO monitor and notify;
    }
}
