build::
	cargo build

S=target/debug/sdmess

P=045f47c3-b681-41eb-87f2-aa53ac23ac1e

test:
	$(S) | grep -q flash
	@echo ✔ help 1
	
	$(S) -h | grep -q flash
	@echo ✔ help 2
	
	$(S) --help | grep -q flash
	@echo ✔ help 3
	
	! $(S) --foo
	@echo ✔ not valid

	$(S) scan
	@echo ✔ scan

	$(S) list | grep -q 'No entries'
	@echo ✔ list is empty
	
	$(S) add test_uuid "desc 1"
	@echo ✔ add

	$(S) list
	@echo ✔ list runs

	$(S) list | grep -q test_uuid
	@echo ✔ list finds

	! $(S) update test_bogus_name "desc 2"
	@echo ✔ update fails

	$(S) update test_uuid "desc 2"
	@echo ✔ update

	$(S) list | grep -q "desc 2"
	@echo ✔ list finds

	$(S) delete test_uuid
	@echo ✔ delete

	$(S) list | grep -q 'No entries'
	@echo ✔ list is empty

test2::
	$(S) scan
	@echo ✔ scan

	$(S) list
	$(S) add $(P) 'desc 3'
	$(S) scan
	$(S) list
	$(S) delete $(P)
